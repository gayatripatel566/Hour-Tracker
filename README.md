# HoursTracker

This Project is about analysis of expected pay of multiple jobs.

It is an Android Application which is developed using 
* Kotlin
* MVVM architecture Data Binding
* Rxjava
* Room - Local Database
* Kotlin Coroutines
* Navigation Components 
* Material Design - RecyclerView, Cardview, StaggeredGridLayout, StickyListHeaders
* Live Data and ViewModels
* Unit Testing.
* Dagger Hilt


### Screenshots
<a href="url"><img src="https://play-lh.googleusercontent.com/xiOemazGKhAMUoLbeLMs2sGEs7c_YTf9fOzr2KtCKl7ouGbsJ0JpoMjUdfzviiSWfA=w1920-h937-rw" align="left" height="400" width="200" ></a>
<a href="url"><img src="https://play-lh.googleusercontent.com/mMe_QHDm2E9RlaP8VWcG0GVx3XsZCPrhWbszTTENe-1B0E4QuPGILxbKPQ3z7Vr9Gd3Q=w1920-h937-rw" align="left" height="400" width="200" ></a>
<a href="url"><img src="https://play-lh.googleusercontent.com/_Ps9N6436XLUTEW7q6GbcoR5kr240Cm5slHC07m7xNU0MdQxUsms_vt8tLyq7OJBWRgE=w1920-h937-rw" align="left" height="400" width="200" ></a>

