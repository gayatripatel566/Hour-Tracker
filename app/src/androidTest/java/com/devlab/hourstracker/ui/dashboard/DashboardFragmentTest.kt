package com.devlab.hourstracker.ui.dashboard

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.devlab.hourstracker.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DashboardFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(DashboardActivity::class.java)

    @Test
    fun testDashboardWorkLogs() {
        onView(withId(R.id.worklogs)).check(matches(withText(R.string.work_logs)))
        onView(withId(R.id.worklogs)).check(matches(isDisplayed()))
        onView(withId(R.id.cardWorkLogs))
        onView(withId(R.id.cardWorkLogs)).perform(click())
    }

    @Test
    fun testDashboardMyJobs() {
        onView(withId(R.id.myJobs)).check(matches(withText(R.string.my_jobs)))
        onView(withId(R.id.cardMyJobs))
        onView(withId(R.id.cardMyJobs)).perform(click())
    }

}