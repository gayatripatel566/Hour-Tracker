package com.devlab.hourstracker.data.source.jobs

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.devlab.hourstracker.data.source.HTDatabase
import com.devlab.hourstracker.getOrAwaitValue
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class EmployerDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: HTDatabase
    private lateinit var dao: EmployerDao

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            HTDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.employerDao()
    }

    @Test
    fun insertEmployerTest() = runBlockingTest {
        val employer = Employer(10, "abc", "weekly", "abccc", 25, "nsw", true)

        dao.insertEmployer(employer)

        val allEmployer = dao.getEmployerList().getOrAwaitValue()
        assertThat(allEmployer).contains(employer)
    }

    @Test
    fun deleteEmployerTest() = runBlockingTest {
        val employer = Employer(10, "abc", "weekly", "abccc", 25, "nsw", true)
        dao.insertEmployer(employer)
        dao.deleteEmployer(employer)

        val allEmployer = dao.getEmployerList().getOrAwaitValue()
        assertThat(allEmployer).doesNotContain(employer)

    }

    @After
    fun tearDown() {
        database.close()
    }
}