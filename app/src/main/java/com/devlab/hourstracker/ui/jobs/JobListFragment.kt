package com.devlab.hourstracker.ui.jobs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.devlab.hourstracker.R
import com.devlab.hourstracker.data.source.jobs.Employer
import com.devlab.hourstracker.databinding.ListEmployerBinding
import com.devlab.hourstracker.util.changeNavigationBarColor
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class JobListFragment : Fragment(), JobAdapter.Listener {

    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var viewDataBinding: ListEmployerBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewDataBinding = ListEmployerBinding.inflate(inflater, container, false).apply {
            viewModel = jobViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
        setUI()
        setUpListAdapter()
        setUpFab()

    }

    private fun setUI() {
        changeNavigationBarColor(requireActivity(), R.color.secondary_text)
        val toolbar = (requireActivity() as AppCompatActivity).supportActionBar
        toolbar?.title = getString(R.string.string_my_jobs)
        toolbar?.show()
        toolbar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setUpFab() {
        viewDataBinding.fab.text = getString(R.string.add_job)
        viewDataBinding.fab.setOnClickListener {
            findNavController().navigate(R.id.action_JobListFragment_to_JobFragment)
        }
    }

    private fun setUpListAdapter() {
        val jobAdapter = JobAdapter(this)
        val recyclerView = viewDataBinding.jobsList
        recyclerView.adapter = jobAdapter
        recyclerView.layoutManager =
            StaggeredGridLayoutManager(
                resources.getInteger(R.integer.column_job),
                StaggeredGridLayoutManager.VERTICAL
            )
        jobViewModel.items.observe(viewLifecycleOwner, { jobs ->
            jobs.let { jobAdapter.setJobs(jobs) }
        })
    }

    override fun onItemClick(employer: Employer, position: Int) {
        val bundle = bundleOf("Employer" to employer)
        findNavController().navigate(R.id.action_JobListFragment_to_JobFragment, bundle)
    }

}