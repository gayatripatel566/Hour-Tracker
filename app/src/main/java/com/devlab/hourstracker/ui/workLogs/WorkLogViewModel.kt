package com.devlab.hourstracker.ui.workLogs

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.devlab.hourstracker.data.source.Result
import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.data.source.worklog.WorkLogRepository
import kotlinx.coroutines.launch
import java.util.*


class WorkLogViewModel @ViewModelInject constructor(private val workLogRepository: WorkLogRepository) :
    ViewModel() {

    private var _itmems = MutableLiveData<List<WorkLog>>()
    var items: LiveData<List<WorkLog>> = workLogRepository.getJobs()

    private var _data = MutableLiveData<ArrayList<HashMap<String, WorkLog>>>()
    val dataList: LiveData<ArrayList<HashMap<String, WorkLog>>> = _data

    val empty: LiveData<Boolean> = Transformations.map(items) {
        it.isEmpty()
    }

    private val _noWorkLogLable = MutableLiveData(" No Worklog data available.")
    val noWorkLogLable: LiveData<String> = _noWorkLogLable

    fun deleteWorkLog(workLog: WorkLog) {
        viewModelScope.launch {
            workLogRepository.deleteWorkLog(workLog)
        }
    }


    private fun filterTasks(tasksResult: Result<List<WorkLog>>): LiveData<List<WorkLog>> {
        val result = MutableLiveData<List<WorkLog>>()
        if (tasksResult is Result.Success) {
            viewModelScope.launch {
                result.value = tasksResult.data
            }
        } else {
            result.value = emptyList()
            //   showSnackbarMessage(R.string.loading_tasks_error)
        }

        return result
    }

    fun getHeaderWithItems(): ArrayList<HashMap<String, WorkLog>> {
        val dataMapList = ArrayList<HashMap<String, WorkLog>>()
        viewModelScope.launch {
            var datamap = HashMap<String, WorkLog>()
            val jobs = workLogRepository.getJobsTotal()
            for (item in jobs.indices) {
                if (item == 0)
                    datamap[jobs[item].jobWeek!!] = jobs[item]
                else if (jobs[item].jobWeek.equals(jobs[item - 1].jobWeek))
                    datamap[jobs[item].jobWeek.toString()] = jobs[item]
                else {
                    datamap = HashMap<String, WorkLog>()
                    datamap[jobs[item].jobWeek.toString()] = jobs[item]
                }
                dataMapList.add(datamap)
            }
            _data.value = dataMapList
        }

        return dataMapList
    }

    private fun filterTaskstoList(tasksResult: LiveData<List<WorkLog>>): List<WorkLog> {
        _itmems.value = workLogRepository.getJobs().value
        items = _itmems
        var result = listOf<WorkLog>()
        items.map { jobs ->
            result = jobs
        }
        return result
    }

}


