package com.devlab.hourstracker.ui.jobAddEdit

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import com.devlab.hourstracker.R
import com.devlab.hourstracker.data.source.jobs.Employer
import com.devlab.hourstracker.databinding.AddEditEmployerBinding
import com.devlab.hourstracker.util.changeNavigationBarColor
import com.devlab.hourstracker.util.snackbar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddEditJobFragment : Fragment() {

    private val viewModel:AddEditJobViewModel by viewModels()
    private lateinit var viewDataBinding: AddEditEmployerBinding
    lateinit var employer: Employer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewDataBinding = AddEditEmployerBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel
        }
        return viewDataBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
        setUI()
        setupSnackbar()
        setUpNavigation()
        setData()
        deleteClick()

    }

    private fun setData() {
        if (arguments != null) {
            employer = arguments?.getSerializable("Employer") as Employer
            viewModel.start(employer)
        } else
            viewModel.start(null)

    }

    private fun setUI() {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        changeNavigationBarColor(requireActivity(), R.color.primary)
    }

    private fun setUpNavigation() {
        viewModel.savingChanges.observe(viewLifecycleOwner, {
            when (it) {
                true -> activity?.onBackPressed()
            }
        })
    }


    private fun deleteClick() {
        viewDataBinding.delImageButton.setOnClickListener {

            MaterialAlertDialogBuilder(requireContext())
                .setTitle("Delete Job")
                .setMessage("Are you sure you want to delete job as well as all work logs ?")
                .setNeutralButton(resources.getString(R.string.cancel)) { dialog, _ ->
                    dialog.cancel()
                }
                .setPositiveButton(resources.getString(R.string.accept)) { _, _ ->
                    viewModel.deleteEmployer(employer)
                }
                .show()
        }
    }


    private fun setupSnackbar() {
        viewModel.snackbarText.observe(viewLifecycleOwner, {
            snackbar(viewDataBinding.root, viewModel.snackbarText.value)
        })
    }



}