package com.devlab.hourstracker.ui.jobAddEdit

object JobUtil {

     fun isValidInputs(
        currentJobTile: String?,
        currentRate: String?,
        currentLocation: String?,
        currentDesignation: String?
    ): DataStatus {

        if (currentJobTile == null || currentJobTile.isEmpty()) {
            return DataStatus(false,"Enter Job Title")
        }
        if (currentRate == null || currentRate.toString().isEmpty()) {
            return DataStatus(false,"Enter Job Rate")
        }
        if (currentDesignation == null || currentDesignation.isEmpty()) {
            return DataStatus(false,"Enter Designation")
        }
        if (currentLocation == null || currentLocation.isEmpty()) {
            return DataStatus(false,"Enter Job Location")
        }
         return DataStatus(true,"")
    }
    data class DataStatus(
        var isvalid: Boolean,
        var message: String
    )
}