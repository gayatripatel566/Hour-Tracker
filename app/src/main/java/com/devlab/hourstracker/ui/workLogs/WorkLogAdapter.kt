package com.devlab.hourstracker.ui.workLogs

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.databinding.RawWorkListBinding

class WorkLogAdapter(private val listener: Listener) :
    RecyclerView.Adapter<WorkLogAdapter.MyViewHolder>() {
    private var jobList = emptyList<WorkLog>()

    interface Listener {
        fun onItemClick(workLog: WorkLog, position: Int)
        fun onLongClick(workLog: WorkLog): Boolean
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RawWorkListBinding.inflate(inflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return jobList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(jobList[position], listener, position)
    }

    internal fun setJobs(workLogs: List<WorkLog>) {
        this.jobList = workLogs
        notifyDataSetChanged()
    }

    inner class MyViewHolder(val binding: RawWorkListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            workLog: WorkLog,
            listener: Listener,
            position: Int
        ) {
            binding.workLog = workLog
            binding.root.setOnClickListener { listener.onItemClick(workLog, position) }
            binding.root.setOnLongClickListener { listener.onLongClick(workLog) }
        }
    }
}