package com.devlab.hourstracker.ui.worklogAddEdit

import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.util.dateTimeSimpleDateFormat
import java.text.SimpleDateFormat
import java.util.*

internal fun hoursCalculation(workLog: WorkLog): HoursAmount {
    val formatStartDate = "${workLog.workStartDate} ${workLog.workStartTime}"
    val startDateTime = dateTimeSimpleDateFormat.parse(formatStartDate)

    val formatEndDate = "${workLog.workEndDate} ${workLog.workEndTime}"
    val endDateTime = dateTimeSimpleDateFormat.parse(formatEndDate)

    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24

    var diff = endDateTime.time - startDateTime.time

    val week = getWeek(startDateTime)

    val totalDays = diff / daysInMilli
    diff %= daysInMilli

    val totalHours = diff / hoursInMilli
    diff %= hoursInMilli

    val totalMinutes = diff / minutesInMilli
    diff %= minutesInMilli

    val totalTime = "$totalDays.$totalHours.$totalMinutes"

    val times = totalTime.split(".")
    var finalTime = ""
    var amount = 0.0
    var isCorrect = true

    if (times.size == 3) {
        val daytime = times[0].toLong() * 24
        val hours = times[1].toLong()
        val minutes = times[2].toLong()

        val totalHours = daytime + hours
        val totalMin = minutes * 100 / 60
        if (totalHours < 0 || totalMin < 0)
            isCorrect = false
        finalTime = "" + totalHours + "h " + minutes + "m"
        amount =
            totalHours * workLog.workRate.toDouble() + (totalMin * workLog.workRate.toDouble()) / 100
    }

    return HoursAmount(finalTime, amount, isCorrect, week)

}

fun getWeek(startDateTime: Date): String {
    val calendar = Calendar.getInstance()
    val simpleDateFormat = SimpleDateFormat("dd MMM")
    calendar.time = startDateTime
    calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
    val startDate: String = simpleDateFormat.format(calendar.time)
    calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY)
    val endDate: String = simpleDateFormat.format(calendar.time)
    return "$startDate to $endDate"
}

data class HoursAmount(
    val totalHours: String,
    val totalAmount: Double,
    val correctTime: Boolean = true,
    val week: String
)
