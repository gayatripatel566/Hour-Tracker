package com.devlab.hourstracker.ui.jobAddEdit

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devlab.hourstracker.data.source.jobs.Employer
import com.devlab.hourstracker.data.source.jobs.EmployerRepository
import com.devlab.hourstracker.ui.jobAddEdit.JobUtil.isValidInputs
import kotlinx.coroutines.launch

class AddEditJobViewModel @ViewModelInject constructor(private val employerRepository: EmployerRepository) :
    ViewModel() {


    private var employerId: Int? = null

    var jobTitle = MutableLiveData<String>()

    var rate = MutableLiveData<String>()

    var schedule = MutableLiveData<String>()

    var location = MutableLiveData<String>()

    var designation = MutableLiveData<String>()

    private lateinit var employer: Employer
    private val _snackbarText = MutableLiveData<String>()
    val snackbarText: LiveData<String> = _snackbarText

    private var isNewEmployer: Boolean = false

    private val _isEdit = MutableLiveData<Boolean>()
    val isEdit: LiveData<Boolean> = _isEdit

    private val _savingChanges = MutableLiveData<Boolean>()
    val savingChanges: LiveData<Boolean> = _savingChanges

    fun start(employer: Employer?) {
        _savingChanges.value = false
        _isEdit.value = false
        if (employer != null) {
            this.employer = employer
            employerId = employer.empId
            _isEdit.value = true
            isNewEmployer = false
            setData(employer)
        }

    }

    private fun setData(employer: Employer) {
        jobTitle.value = employer.employerJobTitle
        rate.value = employer.employerJobRate.toString()
        designation.value = employer.employerJobDesignation
        location.value = employer.employerLocation
        schedule.value = employer.employerJobSchedule
    }

    fun saveEmployer() {
        val currentJobTile = jobTitle.value
        val currentRate = rate.value
        val currentLocation = location.value
        val currentDesignation = designation.value

        val validDetails = isValidInputs(
            currentJobTile,
            currentRate,
            currentLocation,
            currentDesignation
        )
        if (validDetails.isvalid) {

            val employer = Employer(
                null,
                currentJobTile!!,
                "Weekly",
                currentDesignation!!,
                currentRate?.toInt()!!,
                currentLocation!!,
                true
            )

            if (isNewEmployer || employerId == null) {
                insertEmployer(employer)
            } else {
                employer.empId = employerId as Int
                updateEmployer(employer)
            }
        } else {
            _snackbarText.value = validDetails.message
        }
    }


    private fun insertEmployer(employer: Employer) {
        viewModelScope.launch {
            employerRepository.insertEmployer(employer)
            _savingChanges.value = true
        }

    }

    private fun updateEmployer(employer: Employer) {
        viewModelScope.launch {
            employerRepository.updateEmployerJob(employer)
            _savingChanges.value = true
        }
    }

    fun deleteEmployer(employer: Employer) {
        viewModelScope.launch {
            employerRepository.deleteEmployerJob(employer)
            _savingChanges.value = true
        }

    }


}

