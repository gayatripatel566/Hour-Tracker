package com.devlab.hourstracker.ui.jobs

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.devlab.hourstracker.data.source.Result
import com.devlab.hourstracker.data.source.jobs.Employer
import com.devlab.hourstracker.data.source.jobs.EmployerRepository
import kotlinx.coroutines.launch

class JobViewModel @ViewModelInject constructor(repository: EmployerRepository) : ViewModel(){

    private val employerLiveData: LiveData<List<Employer>> = repository.getEmployerList()

    private val _noTasksLabel = MutableLiveData("No Data Available")
    val noTasksLabel: LiveData<String> = _noTasksLabel

    val items: LiveData<List<Employer>> = employerLiveData
    val empty: LiveData<Boolean> = Transformations.map(items) {
        it.isEmpty()
    }



    private fun filterTasks(tasksResult: Result<List<Employer>>): LiveData<List<Employer>> {
        val result = MutableLiveData<List<Employer>>()
        if (tasksResult is Result.Success) {
            viewModelScope.launch {
                result.value = tasksResult.data
            }
        } else {
            result.value = emptyList()
            //   showSnackbarMessage(R.string.loading_tasks_error)
        }

        return result
    }
}
