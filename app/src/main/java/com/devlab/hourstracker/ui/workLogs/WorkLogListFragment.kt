package com.devlab.hourstracker.ui.workLogs

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.devlab.hourstracker.R
import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.databinding.ListWorklogBinding
import com.devlab.hourstracker.util.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class WorkLogListFragment : Fragment(), WorkLogAdapter.Listener {

    //test
    private val workLogViewModel: WorkLogViewModel by viewModels()
    private lateinit var vieDataBinding: ListWorklogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vieDataBinding = ListWorklogBinding.inflate(inflater, container, false).apply {
            viewModel = workLogViewModel
        }
        return vieDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        vieDataBinding.lifecycleOwner = this.viewLifecycleOwner
        setUI()
        setUpListAdapter()
        setUpFab()

    }

    private fun setUI() {
        changeNavigationBarColor(requireActivity(), R.color.secondary_text)
        val toolbar = (requireActivity() as AppCompatActivity).supportActionBar
        toolbar?.title = getString(R.string.work_logs)
        toolbar?.show()
        toolbar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setUpFab() {
        vieDataBinding.fab.text = resources.getString(R.string.add_shift)
        vieDataBinding.fab.setOnClickListener {
            findNavController().navigate(R.id.action_WorkLogList_to_WorkLogFragment)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                showCurrencyDialogue()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onItemClick(workLog: WorkLog, position: Int) {
        val bundle = bundleOf("WorkLog" to workLog)
        findNavController().navigate(R.id.action_WorkLogList_to_WorkLogFragment, bundle)
    }

    override fun onLongClick(workLog: WorkLog): Boolean {

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Are you sure you want to Delete record?")
            .setNegativeButton(resources.getString(R.string.no)) { _, which ->

            }
            .setPositiveButton(resources.getString(R.string.yes)) { _, which ->
                workLogViewModel.deleteWorkLog(workLog)

            }
            .show()
        return true
    }

    private fun setUpListAdapter() {
        val adapter = WorkLogAdapter(this)
        val recyclerView = vieDataBinding.worklogList
        recyclerView.layoutManager = StaggeredGridLayoutManager(
            resources.getInteger(R.integer.column),
            StaggeredGridLayoutManager.VERTICAL
        )
        recyclerView.adapter = adapter

        workLogViewModel.items.observe(viewLifecycleOwner, { jobs ->
            jobs.let {
                adapter.setJobs(jobs)
                val dataList = workLogViewModel.getHeaderWithItems()
                getData(recyclerView, dataList, jobs)
            }
        })
    }

    private fun getData(
        recyclerView: RecyclerView,
        data: ArrayList<HashMap<String, WorkLog>>,
        jobs: List<WorkLog>
    ) {
        val recyclerItemDecoration = RecyclerItemDecoration(
            resources.getDimensionPixelSize(R.dimen.header_height),
            true,
            getSectionCallback(data)
        )
        while (recyclerView.itemDecorationCount > 0) {
            recyclerView.removeItemDecorationAt(0)
        }
        if (data.size == jobs.size)
            recyclerView.addItemDecoration(recyclerItemDecoration)
    }

    private fun getSectionCallback(list: ArrayList<HashMap<String, WorkLog>>): RecyclerItemDecoration.SectionCallback {
        return object : RecyclerItemDecoration.SectionCallback {
            override fun isSection(pos: Int): Boolean =
                !(pos != 0 && list[pos].keys === list[pos - 1].keys)

            override fun getSectionHeaderName(pos: Int): String {
                val dataMap: HashMap<String, WorkLog> = list[pos]
                val key = dataMap.keys.toString()
                val keyStr = key.substring(1, key.length - 1)
                val values = dataMap[keyStr]?.weekTotal
                return "$keyStr,$values"
            }
        }
    }

    private fun showCurrencyDialogue() {
        val listItems = provideCurrencyList()

        val builder: AlertDialog.Builder =
            AlertDialog.Builder(requireActivity())
        builder.setTitle("Choose Currency")
        builder.setItems(listItems) { dialog, which ->
            val currency = listItems[which].split(" - ")
            Prefs.putAny("currency", currency[0])
            setUpListAdapter()
        }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}