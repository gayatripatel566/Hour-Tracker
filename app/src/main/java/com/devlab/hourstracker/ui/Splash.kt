package com.devlab.hourstracker.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.devlab.hourstracker.R
import com.devlab.hourstracker.ui.dashboard.DashboardActivity
import com.devlab.hourstracker.ui.pager.PagerActivity
import com.devlab.hourstracker.util.Prefs
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Splash : AppCompatActivity() {

    private var isFirstTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       setContentView(R.layout.activity_splash)

        setUI()
        navigateToMain()
    }

    private fun setUI() {
        window.navigationBarColor = ContextCompat.getColor(this, R.color.secondary_text)
        isFirstTime = Prefs.getBoolean("first_time", true)
    }

    private fun navigateToMain() {
        val intent: Intent = if (isFirstTime)
            Intent(this, PagerActivity::class.java)
        else
            Intent(this, DashboardActivity::class.java)

        GlobalScope.launch {
            delay(3000L)
            startActivity(intent)
            finish()
        }
    }
}