package com.devlab.hourstracker.ui.dashboard

import android.os.Bundle
import android.transition.Explode
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.devlab.hourstracker.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setAnimation()
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(findViewById(R.id.toolbar))
    }


    private fun setAnimation() {
        val explode = Explode()
        explode.duration = 800
        explode.interpolator = DecelerateInterpolator()
        window.exitTransition = explode
        window.enterTransition = explode
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}