package com.devlab.hourstracker.ui.worklogAddEdit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import com.devlab.hourstracker.R
import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.databinding.AddEditWorklogBinding
import com.devlab.hourstracker.util.changeNavigationBarColor
import com.devlab.hourstracker.util.convertLongToDate
import com.devlab.hourstracker.util.dateSimpleFormat
import com.devlab.hourstracker.util.snackbar
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.CompositeDateValidator
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AddEditWorkLogFragment : Fragment() {


    private val viewModel:AddEditWorkLogViewModel by viewModels()
    private lateinit var databinding: AddEditWorklogBinding
    private var startDate: Date? = null
    private lateinit var endDate: Date
    private lateinit var workLog: WorkLog
    private lateinit var arrayAdapter: ArrayAdapter<Any>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        databinding = AddEditWorklogBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel
        }
        return databinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        databinding.lifecycleOwner = this.viewLifecycleOwner
        setUI()
        setupSnackbar()
        setUpNavigation()
        setData()
        setDropDown()
        setPicker()

    }

    private fun setDropDown() {
        arrayAdapter = ArrayAdapter(
            requireActivity(),
            R.layout.raw_job_title,
            viewModel.names
        )
        databinding.autoWorkTitle.setAdapter(arrayAdapter)
        databinding.autoWorkTitle.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                viewModel.setRate(parent.getItemAtPosition(position).toString())
            }
    }

    private fun setData() {
        workLog = WorkLog()
        if (arguments != null) {
            workLog = arguments?.getSerializable("WorkLog") as WorkLog
            viewModel.start(workLog)
        } else
            viewModel.start(null)

    }

    private fun setUI() {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        changeNavigationBarColor(requireActivity(), R.color.primary)
    }

    private fun setUpNavigation() {
        viewModel.savingChanges.observe(viewLifecycleOwner, {
            when (it) {
                true -> activity?.onBackPressed()
            }
        })
    }

    private fun setupSnackbar() {
        viewModel.snackbarText.observe(viewLifecycleOwner, {
            snackbar(databinding.root, viewModel.snackbarText.value)
        })
    }

    private fun setPicker() {
        databinding.editWorkStartDate.setOnClickListener {
            showDatePickerDialog(viewModel.startDate, true)
        }
        databinding.editWorkEndDate.setOnClickListener {
            showDatePickerDialog(viewModel.endDate, false)
        }
        databinding.editWorkEndTime.setOnClickListener {
            showTimePickerDialog(viewModel.endTime)
        }
        databinding.editWorkStartTime.setOnClickListener {
            showTimePickerDialog(viewModel.startTime)
        }
        databinding.deleteImageButton.setOnClickListener {

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(getString(R.string.delete_string))
                .setNegativeButton(resources.getString(R.string.no)) { dialog, _ ->
                    dialog.cancel()
                }
                .setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                    viewModel.deleteEmployer(workLog)
                }
                .show()
        }
    }

    private fun showDatePickerDialog(date: MutableLiveData<String>, isStartDate: Boolean) {

        val builder: MaterialDatePicker.Builder<*> = MaterialDatePicker.Builder.datePicker()
        builder.setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
        if (startDate != null) {
            val constraintsBuilder = CalendarConstraints.Builder()
            if (!isStartDate) {
                val validators: ArrayList<CalendarConstraints.DateValidator> = ArrayList()
                validators.add(DateValidatorPointForward.from(startDate!!.time))
                constraintsBuilder.setValidator(CompositeDateValidator.allOf(validators))
                builder.setCalendarConstraints(constraintsBuilder.build())
            } else {
                constraintsBuilder.setOpenAt(MaterialDatePicker.todayInUtcMilliseconds())
                builder.setCalendarConstraints(constraintsBuilder.build())
            }
        }

        val picker: MaterialDatePicker<*> = builder.build()
        picker.addOnPositiveButtonClickListener {
            val selectDate = picker.selection as Long
            val formatDate = convertLongToDate(selectDate)
            date.value = dateSimpleFormat.format(formatDate)
            if (isStartDate) startDate = formatDate else endDate = formatDate
        }
        picker.show(activity?.supportFragmentManager!!, picker.toString())
    }

    private fun showTimePickerDialog(time: MutableLiveData<String>) {

        val timePickerDialog = MaterialTimePicker.newInstance()
        timePickerDialog.show(activity?.supportFragmentManager!!, "TimePicker")
        timePickerDialog.setTimeFormat(TimeFormat.CLOCK_24H)
        timePickerDialog.setListener { dialog: MaterialTimePicker ->
            val newHour = dialog.hour
            val newMinute = dialog.minute
            val calender: Calendar = Calendar.getInstance()
            calender.set(Calendar.HOUR_OF_DAY, newHour)
            calender.set(Calendar.MINUTE, newMinute)
            val timeFormat = "hh:mm a"
            val simpleFormats = SimpleDateFormat(timeFormat, Locale.getDefault())
            time.value = simpleFormats.format(calender.time)
        }
    }
}