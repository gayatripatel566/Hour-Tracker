package com.devlab.hourstracker.ui.pager

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager

import com.devlab.hourstracker.R
import com.devlab.hourstracker.databinding.ActivityPagerBinding
import com.devlab.hourstracker.ui.dashboard.DashboardActivity
import com.devlab.hourstracker.util.Prefs
import com.devlab.hourstracker.util.putAny
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PagerActivity : AppCompatActivity(), View.OnClickListener {

    private var currentPage = 0
    private lateinit var indicators: Array<ImageView>
    private lateinit var viewPager: ViewPager
    lateinit var binding: ActivityPagerBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPagerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.navigationBarColor = ContextCompat.getColor(this, R.color.secondary_text)
        Prefs.putAny("first_time", false)

        setViewPager()

        binding.introBtnNext.setOnClickListener(this)
        binding.introBtnSkip.setOnClickListener(this)
    }

    private fun setViewPager() {
        indicators = arrayOf(
            binding.introIndicator0,
            findViewById<View>(R.id.intro_indicator_1) as ImageView,
            findViewById<View>(R.id.intro_indicator_2) as ImageView
        )
        val sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                currentPage = position
                updateIndicator(position)

            }
        })
    }

    private fun updateIndicator(position: Int) {
        for (i in indicators.indices) {
            if (i == position) {
                indicators[i].setImageResource(R.drawable.indicator_selected)
            } else indicators[i].setImageResource(R.drawable.indicator_unselected)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.intro_btn_skip -> {
                val intent = Intent(this, DashboardActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.intro_btn_next -> {
                when (currentPage) {
                    1 -> {
                        viewPager.currentItem = currentPage + 1
                        binding.introBtnNext.text = getString(R.string.finish)
                    }
                    2 -> {
                        val intent = Intent(this, DashboardActivity::class.java)
                        val options = ActivityOptions.makeSceneTransitionAnimation(this)
                        startActivity(intent, options.toBundle())
                        finish()
                    }
                    else -> {
                        viewPager.currentItem = currentPage + 1
                    }
                }
            }
        }

    }

}

