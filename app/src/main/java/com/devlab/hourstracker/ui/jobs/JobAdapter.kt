package com.devlab.hourstracker.ui.jobs

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.devlab.hourstracker.R
import com.devlab.hourstracker.data.source.jobs.Employer
import com.devlab.hourstracker.databinding.RawJobListBinding
import java.util.*


class JobAdapter(private val listener: Listener) : RecyclerView.Adapter<JobAdapter.MyViewHolder>() {
    private var jobList = emptyList<Employer>()
    private var colorList = arrayOf("#e20147", "#0037ae", "#40B640")
    private val mRandom = Random()

    interface Listener {
        fun onItemClick(employer: Employer, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RawJobListBinding.inflate(inflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return jobList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(jobList[position], colorList, listener, position)
    }

    internal fun setJobs(employer: List<Employer>) {
        this.jobList = employer
        notifyDataSetChanged()
    }

    private fun getRandomIntInRange(max: Int, min: Int): Int {
        return mRandom.nextInt(max - min) + min
    }

    inner class MyViewHolder(val binding: RawJobListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            employer: Employer,
            colorList: Array<String>,
            listener: Listener,
            position: Int
        ) {
            binding.employer = employer
            binding.jobListCard.layoutParams.height = getRandomIntInRange(600, 450)
            val card = binding.root.findViewById<CardView>(R.id.jobListCard)
            card.setCardBackgroundColor(Color.parseColor(colorList[position % 3]))
            binding.root.setOnClickListener { listener.onItemClick(employer, position) }
        }
    }
}