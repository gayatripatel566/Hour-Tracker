package com.devlab.hourstracker.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.devlab.hourstracker.R
import com.devlab.hourstracker.databinding.FragmentDashboardBinding
import com.devlab.hourstracker.ui.jobs.JobViewModel
import com.devlab.hourstracker.util.changeNavigationBarColor
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : Fragment() {

    lateinit var binding: FragmentDashboardBinding
    private val jobViewModel: JobViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUI()
        setClick()
    }

    private fun setUI() {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        changeNavigationBarColor(requireActivity(), R.color.colorOnPrimary)
        jobViewModel.empty.observe(viewLifecycleOwner, {
            if (it) {
                binding.cardWorkLogs.visibility = View.GONE
                binding.myJobs.text=getString(R.string.add_job)
            }
        })
    }

    private fun setClick() {
        binding.cardWorkLogs.setOnClickListener {
            findNavController().navigate(R.id.action_DashboardFragment_to_WorkLogListFragment)
        }
        binding.cardMyJobs.setOnClickListener {
            findNavController().navigate(R.id.action_DashboardFragment_to_JobListFragment)
        }
    }


}