package com.devlab.hourstracker.ui.worklogAddEdit

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.data.source.worklog.WorkLogRepository
import kotlinx.coroutines.launch

class AddEditWorkLogViewModel @ViewModelInject constructor(private val workLogRepository: WorkLogRepository) :
    ViewModel() {


//    private val employerRepository: EmployerRepository
//
//    init {
//        employerRepository = EmployerRepository(employerDao)
//    }

    private var shiftId: Int? = null

    var jobTitle = MutableLiveData<String>()

    var rate = MutableLiveData<String>()

    var startDate = MutableLiveData<String>()

    var endDate = MutableLiveData<String>()

    var startTime = MutableLiveData<String>()

    var endTime = MutableLiveData<String>()

    private var totalAmount = MutableLiveData<String>()

    var workLog = WorkLog()

    var names: List<String> = workLogRepository.getEmployerNameList()

    private val _snackbarText = MutableLiveData<String>()
    val snackbarText: LiveData<String> = _snackbarText

    private var isNewEntry: Boolean = false

    private val _isEdit = MutableLiveData<Boolean>()
    val isEdit: LiveData<Boolean> = _isEdit

    private val _savingChanges = MutableLiveData<Boolean>()
    val savingChanges: LiveData<Boolean> = _savingChanges


    fun start(workLog: WorkLog?) {
        _savingChanges.value = false
        _isEdit.value = false
        if (workLog != null) {
            this.workLog = workLog
            shiftId = workLog.workID
            _isEdit.value = true
            isNewEntry = false
            setData(workLog)
        }

    }

    private fun setData(workLog: WorkLog) {
        jobTitle.value = workLog.workTitle
        rate.value = workLog.workRate.toString()
        startDate.value = workLog.workStartDate
        endDate.value = workLog.workEndDate
        startTime.value = workLog.workStartTime
        endTime.value = workLog.workEndTime
        totalAmount.value = workLog.workTotalAmount
    }

    fun setRate(name: String) {
        viewModelScope.launch {
            val selectedRate = workLogRepository.getRate(name)
            rate.value = selectedRate
        }
    }


    fun saveEmployer() {

        val currentTitle = jobTitle.value.toString()
        val currentRate = rate.value.toString()
        val currentStartDate = startDate.value
        val currentEndDate = endDate.value
        val currentStartTime = startTime.value
        val currentEndTime = endTime.value


        if (currentTitle == "null" || currentTitle.isEmpty()) {
            _snackbarText.value = "Please select Job Title"
            return
        }
        if (currentRate == "null" || currentRate.isEmpty()) {
            _snackbarText.value = "Enter Job Rate"
            return
        }
        if (currentStartDate == null || currentStartDate.isEmpty()) {
            _snackbarText.value = "Please select Start Date"
            return
        }
        if (currentEndDate == null || currentEndDate.isEmpty()) {
            _snackbarText.value = "Please select End Date."
            return
        }
        if (currentStartTime == null || currentStartTime.isEmpty()) {
            _snackbarText.value = "Please select Start Time"
            return
        }
        if (currentEndTime == null || currentEndTime.isEmpty()) {
            _snackbarText.value = "Please select End Time"
            return
        }

        val workLog = WorkLog()
        workLog.workTitle = currentTitle
        workLog.workRate = currentRate.toInt()
        workLog.workStartDate = currentStartDate
        workLog.workEndDate = currentEndDate
        workLog.workStartTime = currentStartTime
        workLog.workEndTime = currentEndTime
        val hourAmount = hoursCalculation(workLog)
        if (hourAmount.correctTime) {
            workLog.workTotalTime = hourAmount.totalHours
            workLog.workTotalAmount = hourAmount.totalAmount.toString()
            workLog.jobWeek = hourAmount.week
        } else {
            _snackbarText.value = "Please select correct Date and Time"
            return
        }

        if (isNewEntry || shiftId == null) {
            insertWorkLog(workLog)
        } else {
            workLog.workID = shiftId as Int
            updateWorkLog(workLog)
        }
    }

    private fun insertWorkLog(workLog: WorkLog) {
        viewModelScope.launch {
            workLogRepository.insertJob(workLog)
            _savingChanges.value = true
        }
    }

    private fun updateWorkLog(workLog: WorkLog) {
        viewModelScope.launch {
            workLogRepository.updateWorkLog(workLog)
            _savingChanges.value = true
        }
    }


    fun deleteEmployer(workLog: WorkLog) {
        viewModelScope.launch {
            workLogRepository.deleteWorkLog(workLog)
            _savingChanges.value = true
        }

    }
}

