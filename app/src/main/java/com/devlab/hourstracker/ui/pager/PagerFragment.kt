package com.devlab.hourstracker.ui.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.devlab.hourstracker.R
import com.devlab.hourstracker.databinding.FragmentPagerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PagerFragment : Fragment() {

    private lateinit var bindings: FragmentPagerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bindings = FragmentPagerBinding.inflate(inflater, container, false)
        val onBoardingImg = arrayOf(
            R.drawable.pager1,
            R.drawable.pager2,
            R.mipmap.ic_launcher
        )

        val mainText =
            arrayOf("Add Jobs", "Add Shifts", "View Log")
        val subText =
            arrayOf(
                "Add Your Current Company Name and Pay Rate",
                "Add Shift Entry with Start Time and End Time",
                "View your records by total hours and total payable amount"
            )
        val fragPos = arguments?.getInt(ARG_SECTION_NUMBER)
        bindings.mainTxt.text = mainText[fragPos!!]
        bindings.subTxt.text = subText[fragPos]
        bindings.imageView.setImageDrawable(
            ContextCompat.getDrawable(
                activity?.applicationContext!!,
                onBoardingImg[fragPos]
            )
        )

        return bindings.root
    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PagerFragment {
            return PagerFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)

                }
            }
        }
    }

}