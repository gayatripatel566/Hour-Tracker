package com.devlab.hourstracker.util

import android.content.SharedPreferences
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.devlab.hourstracker.R
import com.google.android.material.snackbar.Snackbar


inline fun snackbar(view: View, message: String?) {
    val snackbar = Snackbar.make(view, message.toString(), Snackbar.LENGTH_LONG).apply {
        setTextColor(ContextCompat.getColor(view.context, R.color.colorOnSecondary))
        setBackgroundTint(ContextCompat.getColor(view.context, R.color.secondary))
    }
    val textView = snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
    textView.textAlignment = View.TEXT_ALIGNMENT_CENTER
    snackbar.show()
}
fun SharedPreferences.putAny(name: String, any: Any) {
    when (any) {
        is String -> edit().putString(name, any).apply()
        is Int -> edit().putInt(name, any).apply()
        is Boolean -> edit().putBoolean(name, any).apply()
    }
}
