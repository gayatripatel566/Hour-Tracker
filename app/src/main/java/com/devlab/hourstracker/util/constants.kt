package com.devlab.hourstracker.util

import android.app.Activity
import android.content.SharedPreferences
import androidx.core.content.ContextCompat
import java.text.SimpleDateFormat
import java.util.*

val dateTimeSimpleDateFormat = SimpleDateFormat("MMM dd, yyyy hh:mm aaa", Locale.getDefault())
val dateSimpleFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
lateinit var Prefs: SharedPreferences

fun convertLongToDate(time: Long): Date {
    val date = Date(time)
    val format = SimpleDateFormat("MMM dd, yyyy")
    val dateString = format.format(date)
    return format.parse(dateString)
}

fun changeNavigationBarColor(context: Activity, colorID: Int) {
    context.window.navigationBarColor = ContextCompat.getColor(context, colorID)
}

fun provideCurrencyList(): Array<String> {
    return arrayOf("Ruble - ₽", "€ - Euro", "$ - dollar", "₹ - rupee","L - lek","£ - pound","Kz - kwanza","֏ - dram","ƒ - florin"
    ,"₼ - manat","¥ - yuan, yen","₽ - ruble","₺ - lira", "₴ - hryvna")
}

