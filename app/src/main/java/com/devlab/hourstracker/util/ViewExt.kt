package com.devlab.hourstracker.util

import android.view.View
import androidx.core.content.ContextCompat
import com.devlab.hourstracker.R
import com.google.android.material.snackbar.Snackbar

fun View.showSnackbar(snackbarText: String, timeLength: Int) {
    Snackbar.make(this, snackbarText, timeLength).run {
        setTextColor(ContextCompat.getColor(view.context, R.color.colorOnSecondary))
        setBackgroundTint(ContextCompat.getColor(view.context, R.color.secondary))
        show()
    }
}

