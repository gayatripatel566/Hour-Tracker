package com.devlab.hourstracker.di

import android.content.Context
import com.devlab.hourstracker.data.source.HTDatabase
import com.devlab.hourstracker.data.source.jobs.EmployerDao
import com.devlab.hourstracker.data.source.jobs.EmployerRepository
import com.devlab.hourstracker.data.source.worklog.WorkLogDao
import com.devlab.hourstracker.data.source.worklog.WorkLogRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {


//    @Singleton
//    @Provides
//    fun provideCharacterRemoteDataSource(characterService: ApiService) = NewsRemoteDataSource(characterService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        HTDatabase.getInstance(appContext)

    @Singleton
    @Provides
    fun provideJobDao(db: HTDatabase) = db.employerDao()

    @Singleton
    @Provides
    fun provideWorkLogDao(db: HTDatabase) = db.jobDao()

    @Singleton
    @Provides
    fun provideEmployerRepository(
        localDataSource: EmployerDao
    ) = EmployerRepository(localDataSource)

    @Singleton
    @Provides
    fun provideWorkLogRepository(
        localDataSource: WorkLogDao
    ) = WorkLogRepository(localDataSource)
}