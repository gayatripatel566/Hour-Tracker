package com.devlab.hourstracker.data.source.worklog

import androidx.lifecycle.LiveData

interface WorkLogDataSource {

    fun getJobs(): LiveData<List<WorkLog>>

    fun getJobsTotal(): List<WorkLog>

    suspend fun insertJob(workLog: WorkLog)

    suspend fun deleteWorkLog(workLog: WorkLog)

    suspend fun updateWorkLog(workLog: WorkLog)

    suspend fun deleteWorkLogByJobTitle(jobTitle: String)

    suspend fun getRate(name: String): String

    fun getEmployerNameList(): List<String>
}