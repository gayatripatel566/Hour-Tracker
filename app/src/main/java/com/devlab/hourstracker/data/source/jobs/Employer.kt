package com.devlab.hourstracker.data.source.jobs

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "employer")
data class Employer(
    @PrimaryKey(autoGenerate = true)
    var empId: Int? = null,
    var employerJobTitle: String,
    var employerJobSchedule: String,
    var employerJobDesignation: String,
    var employerJobRate: Int,
    var employerLocation: String,
    var isActivate: Boolean
): Serializable{
    fun employerJobRateStr(): String {
        return employerJobRate.toString()
    }
}