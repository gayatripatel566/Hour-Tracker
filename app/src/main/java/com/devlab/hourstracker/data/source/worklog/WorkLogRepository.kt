package com.devlab.hourstracker.data.source.worklog

import androidx.lifecycle.LiveData
import javax.inject.Inject

class WorkLogRepository @Inject constructor(private val workLogDao: WorkLogDao) :
    WorkLogDataSource {

    override fun getJobs(): LiveData<List<WorkLog>> {
        return workLogDao.getJobs()
    }

    override fun getJobsTotal(): List<WorkLog> {
        return workLogDao.getJobsTotal()
    }

    override suspend fun insertJob(workLog: WorkLog) {
        workLogDao.insertJob(workLog)
    }

    override suspend fun deleteWorkLog(workLog: WorkLog) {
        workLogDao.deleteWorkLog(workLog)
    }

    override suspend fun updateWorkLog(workLog: WorkLog) {
        workLogDao.updateWorkLog(workLog)
    }

    override suspend fun deleteWorkLogByJobTitle(jobTitle: String) {
        workLogDao.deleteWorkLogByJobTitle(jobTitle)
    }

    override suspend fun getRate(name: String): String {
        return workLogDao.getRate(name).toString()
    }

    override fun getEmployerNameList(): List<String> {
        return workLogDao.getNameList()
    }

}