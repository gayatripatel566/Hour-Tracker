package com.devlab.hourstracker.data.source.worklog

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WorkLogDao {

    @Query("SELECT a.workID,a.job_title,a.job_rate,a.job_start_date,a.job_end_date,a.start_time,a.end_time,a.total_time,a.total_amount,sum(b.total_amount) as week_total,a.week FROM job a,job b where a.week = b.week group by a.week,a.workID order by a.job_start_date desc")
    fun getJobs(): LiveData<List<WorkLog>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertJob( workLog: WorkLog)

    @Delete
    suspend fun deleteWorkLog( workLog: WorkLog)

    @Update
    suspend fun updateWorkLog( workLog: WorkLog)

    @Query("DELETE from job where job_title in (:jobTitle)")
    suspend fun deleteWorkLogByJobTitle(jobTitle: String)

    @Query("SELECT employerJobRate FROM employer where employerJobTitle = (:name)")
    fun getRate(name: String): Int

    @Query("SELECT employerJobTitle FROM employer")
    fun getNameList(): List<String>

    @Query("SELECT a.workID,a.job_title,a.job_rate,a.job_start_date,a.job_end_date,a.start_time,a.end_time,a.total_time,a.total_amount,sum(b.total_amount) as week_total,a.week FROM job a,job b where a.week = b.week group by a.week,a.workID order by a.job_start_date desc")
    fun getJobsTotal(): List<WorkLog>

}