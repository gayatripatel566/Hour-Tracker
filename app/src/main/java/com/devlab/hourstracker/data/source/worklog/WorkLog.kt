package com.devlab.hourstracker.data.source.worklog

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.devlab.hourstracker.util.Prefs
import java.io.Serializable

@Entity(tableName = "job")
class WorkLog:Serializable {

    @PrimaryKey(autoGenerate = true)
    var workID: Int = 0

    @ColumnInfo(name = "job_title")
    var workTitle: String? = null

    @ColumnInfo(name = "job_rate")
    var workRate: Int = 0

    @ColumnInfo(name = "job_start_date")
    var workStartDate: String? = null

    @ColumnInfo(name = "job_end_date")
    var workEndDate: String? = null

    @ColumnInfo(name = "start_time")
    var workStartTime: String? = null

    @ColumnInfo(name = "end_time")
    var workEndTime: String? = null

    @ColumnInfo(name = "total_time")
    var workTotalTime: String? = null

    @ColumnInfo(name = "week")
    var jobWeek: String? = null

    @ColumnInfo(name = "total_amount")
    var workTotalAmount: String? = null

    @ColumnInfo(name = "week_total")
    var weekTotal: String? = null

    fun totalAmtStr(): String {
        return Prefs.getString("currency","$")+workTotalAmount
    }

}