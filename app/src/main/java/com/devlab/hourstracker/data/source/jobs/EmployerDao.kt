package com.devlab.hourstracker.data.source.jobs

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface EmployerDao {

    @Query("SELECT * FROM employer where isActivate = 1")
    fun getEmployerList(): LiveData<List<Employer>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmployer(employer: Employer)

    @Update
    suspend fun updateEmployerJob(employer: Employer)

    @Delete
    suspend fun deleteEmployer(employer: Employer)

    @Query("Update employer set isActivate = 0 where empId in (:jobID)")
    suspend fun deactivateJob(jobID: Int)

}