package com.devlab.hourstracker.data.source

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.devlab.hourstracker.data.source.jobs.Employer
import com.devlab.hourstracker.data.source.worklog.WorkLog
import com.devlab.hourstracker.data.source.jobs.EmployerDao
import com.devlab.hourstracker.data.source.worklog.WorkLogDao


@Database(entities = [WorkLog::class, Employer::class],version = 1, exportSchema = false)
abstract class HTDatabase: RoomDatabase() {
    abstract fun jobDao(): WorkLogDao
    abstract fun employerDao(): EmployerDao

    companion object {
        @Volatile
        private var instance: HTDatabase? = null

        fun getInstance(context: Context): HTDatabase =
            instance ?: synchronized(this){
                instance
                    ?: buildDatabase(context)
                        .also {
                            instance = it
                        }
            }
        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                HTDatabase::class.java,
                "HoursTracking.db"
            )
                .allowMainThreadQueries()
                .build()
    }
}