package com.devlab.hourstracker.data.source.jobs

import androidx.lifecycle.LiveData

interface EmployerDataSource {

    fun getEmployerList(): LiveData<List<Employer>>

    suspend fun insertEmployer(employer: Employer)

    suspend fun updateEmployerJob(employer: Employer)

    suspend fun deleteEmployerJob(employer: Employer)

    suspend fun deactivateJob(jobID: Int)

    //fun getEmployerNameList(): List<String>

    //suspend fun getRate(name: String): String

}