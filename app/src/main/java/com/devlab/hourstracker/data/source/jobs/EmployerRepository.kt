package com.devlab.hourstracker.data.source.jobs

import androidx.lifecycle.LiveData
import javax.inject.Inject

class EmployerRepository @Inject constructor(private val employerDao: EmployerDao) :
    EmployerDataSource {

    override fun getEmployerList(): LiveData<List<Employer>> {
        return employerDao.getEmployerList()
    }

    override suspend fun insertEmployer(employer: Employer) {
        employerDao.insertEmployer(employer)
    }

    override suspend fun updateEmployerJob(employer: Employer) {
        employerDao.updateEmployerJob(employer)
    }

    override suspend fun deleteEmployerJob(employer: Employer) {
        employerDao.deleteEmployer(employer)
    }

    override suspend fun deactivateJob(jobID: Int) {
        employerDao.deactivateJob(jobID)
    }
}