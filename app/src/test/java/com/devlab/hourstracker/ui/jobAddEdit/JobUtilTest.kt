package com.devlab.hourstracker.ui.jobAddEdit

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class JobUtilTest{

    @Test
    fun `valid data returns true`() {

        val result = JobUtil.isValidInputs("Abc", "50", "Sydney", "Developer")

        assertThat(result.isvalid).isTrue()

        assertThat(result.message).isEmpty()

    }

    @Test
    fun `empty data returns false`() {

        val result = JobUtil.isValidInputs("", "", "", "")

        assertThat(result.isvalid).isFalse()

        assertThat(result.message).isNotEmpty()

        assertThat(result.message).contains("Enter Job Title")
    }

    @Test
    fun `empty title returns false`() {

        val result = JobUtil.isValidInputs("", "50", "Sydney", "Developer")

        assertThat(result.isvalid).isFalse()

        assertThat(result.message).isEqualTo("Enter Job Title")

    }
    @Test
    fun `empty rate returns false`() {

        val result = JobUtil.isValidInputs("Abc", "", "Sydney", "Developer")

        assertThat(result.isvalid).isFalse()

        assertThat(result.message).isEqualTo("Enter Job Rate")

    }

    @Test
    fun `empty location returns false`() {

        val result = JobUtil.isValidInputs("Abc", "50", "", "Developer")

        assertThat(result.isvalid).isFalse()

        assertThat(result.message).isEqualTo("Enter Job Location")

    }

    @Test
    fun `empty designation returns false`() {

        val result = JobUtil.isValidInputs("Abc", "50", "Sydney", "")

        assertThat(result.isvalid).isFalse()

        assertThat(result.message).isEqualTo("Enter Designation")

    }


}