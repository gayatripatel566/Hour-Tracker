package com.devlab.hourstracker.ui.worklogAddEdit

import com.devlab.hourstracker.data.source.worklog.WorkLog
import org.junit.Test

import org.junit.Assert.*

class HoursUtilKtTest {

    @Test
    fun hoursCalculation_startEndTime_returnsHoursAmount() {

        val workLog = WorkLog()
        workLog.workStartDate = "Oct 23, 2020"
        workLog.workEndDate = "Oct 23, 2020"
        workLog.workStartTime = "03:00 AM"
        workLog.workEndTime = "06:00 AM"
        workLog.workRate = 10

        val result = hoursCalculation(workLog)

        assertEquals("3h 0m", result.totalHours)
        assertEquals(30.0, result.totalAmount,0.0)
        assertEquals(true, result.correctTime)

    }

    @Test
    fun hoursCalculation_sameTime_returnsZero() {

        val workLog = WorkLog()
        workLog.workStartDate = "Oct 23, 2020"
        workLog.workEndDate = "Oct 23, 2020"
        workLog.workStartTime = "03:00 AM"
        workLog.workEndTime = "03:00 AM"

        val result = hoursCalculation(workLog)

        assertEquals("0h 0m", result.totalHours)
        assertEquals(0.0, result.totalAmount,0.0)
        assertEquals(true, result.correctTime)


    }

    @Test
    fun hoursCalculation_wrongTime_inCorrectDateTime() {

        val workLog = WorkLog()
        workLog.workStartDate = "Oct 23, 2020"
        workLog.workEndDate = "Oct 23, 2020"
        workLog.workStartTime = "03:00 AM"
        workLog.workEndTime = "02:00 AM"

        val result = hoursCalculation(workLog)

        assertEquals("-1h 0m", result.totalHours)
        assertEquals(0.0, result.totalAmount,0.0)
        assertEquals(false, result.correctTime)

    }

}